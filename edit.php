<?php

$db = new PDO('mysql:host=localhost;dbname=project;charset=utf8mb4', 'root', '');


$query = "SELECT * FROM `users` WHERE id = ".$_GET["single"];
$stmt = $db->query($query);
$results = $stmt->fetch(PDO::FETCH_ASSOC);

var_dump($results);
?>


<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <title>Bootstrap 101 Template</title>

    <!-- Bootstrap -->
    <!-- Latest compiled and minified CSS -->
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">

        <!-- Optional theme -->
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap-theme.min.css" integrity="sha384-rHyoN1iRsVXV4nD0JutlnGaslCJuC7uwjduW9SVrLvRYooPp2bWYgmgJQIXwl/Sp" crossorigin="anonymous">

    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
  </head>
  <body>
    <div class="container">
        <div class="col-md-3"></div>
        <div class="col-md-6">
            <form action="insert.php" method="POST" enctype="multipart/form-data">
              <div class="form-group">
                <label for="exampleInputEmail1">Name</label>
                <input type="name" class="form-control" value="<?php echo $results['name']?>" name="name">
              </div>
              <div class="form-group">
                <label for="exampleInputEmail1">Email address</label>
                <input type="email" class="form-control" value="<?php echo $results['email']?>" name="email" placeholder="Email">
              </div>
              <div class="form-group">
                <label for="exampleInputPassword1">Password</label>
                <input type="text" class="form-control" value="<?php echo $results['password']?>" name="password" placeholder="Password">
              </div>
              <div class="form-group">
                <label for="exampleInputEmail1">address</label>
                <textarea name="address" class="form-control" placeholder="address" ><?php echo $results['address']?></textarea>
              </div>
              <div class="form-group">
                <label for="exampleInputPassword1">mobile</label>
                <input type="text" name="mobile" value="<?php echo $results['mobile']?>" class="form-control" placeholder="mobile">
              </div>
              <div class="form-group">
                <label for="exampleInputPassword1">gender</label>
                <?php
                  $gender = $results['gender'];
//                  $gender = 'Female';
                ?>
                <label>
                    <input type="radio" <?php echo($gender=='male')?'checked':'' ?> value="male">
                    male
                    <input type="radio" <?php echo($gender=='female')?'checked':'' ?> value="female">
                    female
                  </label>
              </div>
              <div class="form-group">
                <label for="exampleInputPassword1">hobby</label>
                <input type="checkbox" value="cricket" name="hobby[]" >
                cricket
                <input type="checkbox" value="writing" name="hobby[]" >
                writing
                <input type="checkbox" value="coding" name="hobby[]" >
                coding
              </div>
              <div class="form-group">
                <select class="form-control" name="day">
                  <option value="1">1</option>
                  <option value="2">2</option>
                  <option value="3">3</option>
                  <option value="4">4</option>
                  <option value="5">5</option>
                </select>
                <select class="form-control" name="month">
                  <option value="01">1</option>
                  <option value="02">2</option>
                  <option value="03">3</option>
                  <option value="04">4</option>
                  <option value="05">5</option>
                </select>
                <select class="form-control" name="year">
                  <option value="2001">2001</option>
                  <option value="2002">2002</option>
                  <option value="2003">2003</option>
                  <option value="2004">2004</option>
                  <option value="2005">2005</option>
                </select>
              </div>
              
              <div class="form-group">
                <label for="exampleInputFile">File input</label>
                <input type="file" id="exampleInputFile" name="image">
                <p class="help-block">Example block-level help text here.</p>
              </div>
              <button type="submit" class="btn btn-default">Submit</button>
              <button type="reset" class="btn btn-default">Reset</button>
            </form>
        </div>
    </div>

    <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
    <!-- Include all compiled plugins (below), or include individual files as needed -->
    <!-- Latest compiled and minified JavaScript -->
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>
  </body>
</html>