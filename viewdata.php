<?php
//$db = new PDO('mysql:host=localhost;dbname=student;charset=utf8mb4', 'root', '');
//$query = "SELECT * FROM `users`";
//$stmt = $db->query($query);
//$results = $stmt->fetchAll(PDO::FETCH_ASSOC);
//
////var_dump($results);


    $db = new PDO('mysql:host=localhost;dbname=project;charset=utf8mb4', 'root', '');
    $query = "SELECT * FROM `users`";
    $stmt = $db->query($query);
    $results = $stmt->fetchAll(PDO::FETCH_ASSOC);
?>



<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <title>Bootstrap 101 Template</title>

    <!-- Bootstrap -->
    <!-- Latest compiled and minified CSS -->
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">

        <!-- Optional theme -->
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap-theme.min.css" integrity="sha384-rHyoN1iRsVXV4nD0JutlnGaslCJuC7uwjduW9SVrLvRYooPp2bWYgmgJQIXwl/Sp" crossorigin="anonymous">

    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
  </head>
  <body>
    <div class="container">
        <table class="table table-bordered">
            <thead>
                <tr>
                    <td>id</td>
                    <td>name</td>
                    <td>email</td>
                    <td>password</td>
                    <td>address</td>
                    <td>mobile</td>
                    <td>gender</td>
                    <td>hobby</td>
                    <td>image_name</td>
                    <td>date of birth</td>
                    <td>action</td>
                </tr>
            </thead>
            <tbody>
            <?php
                $s = 1;
                foreach($results as $user){
                    
                   
            ?>
                <tr>
                    <td><?php echo $s++ ?> </td>
                    <td><?php echo $user['name']?></td>
                    <td><?php echo $user['email']?></td>
                    <td><?php echo $user['password']?></td>
                    <td><?php echo $user['address']?></td>
                    <td><?php echo $user['mobile']?></td>
                    <td><?php echo $user['gender']?></td>
                    <td><?php echo $user['hobby']?></td>
                    <td><?php echo $user['image_name']?></td>
                    <td><?php echo $user['dob']?></td>
                    <td>
                        <a href="viewpersonal.php?single=<?php echo $user['id']?>">view</a>
                        <a class="text-danger" href="delete.php?single=<?php echo $user['id']?>">delete</a>
                        <a href="edit.php?single=<?php echo $user['id']?>">edit</a>
                    </td>
                </tr>
            <?php } ?>
            </tbody>
        </table>
    </div>

    <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
    <!-- Include all compiled plugins (below), or include individual files as needed -->
    <!-- Latest compiled and minified JavaScript -->
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>
  </body>
</html>